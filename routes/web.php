<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Frontend
Route::get('/', 'HomeController@index');

//Backend
Route::get('/xadmin/user', 'AdminController@user');
Route::get('/xadmin/index', 'AdminController@index');
Route::post('/xadmin/login', 'AdminController@login');
Route::get('/xadmin/logout', 'AdminController@logout');

//post-category
Route::get('/xadmin/post-category/index', 'PostCategoryController@index');
Route::get('/xadmin/post-category/form', 'PostCategoryController@form');
Route::post('/xadmin/post-category/add', 'PostCategoryController@add');






