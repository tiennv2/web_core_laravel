<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\requests;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

session_start();

class PostCategoryController extends Controller
{
    public function index(){
    	$index 	= DB::table('post_category')->get();
    	$items		= view('admin.post-category.index')->with('index', $index);
        return view('admin_layout')->with('admin.post-caegory.index', $items);
    }

    public function form(){
        return view('admin.post-category.form');
    }

    public function add(Request $request){
    	$data = array();
    	$data['name'] 				= $request->name;
    	$data['alias'] 				= str_slug($request->name);
    	$data['description'] 		= $request->description;
    	$data['image'] 				= $request->image;
    	$data['layout'] 			= $request->layout;
    	$data['ordering'] 			= $request->ordering;
    	$data['status'] 			= $request->status;
    	$data['meta_url'] 			= $request->meta_url;
    	$data['meta_title'] 		= $request->meta_title;
    	$data['meta_keywords'] 		= $request->meta_keywords;
    	$data['meta_description'] 	= $request->meta_description;
    	$data['box_hot'] 			= $request->box_hot;
    	$data['box_highlight'] 		= $request->box_highlight;
    	$data['box_left'] 			= $request->box_left;
    	$data['box_footer'] 		= $request->box_footer;
    	$data['box_right'] 			= $request->box_right;
    	$data['parent'] 			= $request->parent;
    	$data['left'] 				= $request->left;
    	$data['right'] 				= $request->right;

        $result = DB::table('post_category') -> insert($data);
        Session::put('message', 'Dữ liệu đã đuọc cập nhật thành công');
        return Redirect::to('/xadmin/post-category/index');
    }
}
