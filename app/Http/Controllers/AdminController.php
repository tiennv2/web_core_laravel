<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\requests;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

session_start();

class AdminController extends Controller{
    public function user(){
        return view('admin_login');
    }

    public function index(){
        return view('admin.index');
    }

    public function login(Request $request){
        $user_name   = $request->username;
        $password    = md5($request->password);

        $result = DB::table('user_admin') -> where('username', $user_name) -> where ('password', $password)->first();
        if($result){
            Session::put('username', $result->username);
            Session::put('id', $result->id);
            return Redirect::to('/xadmin/index');
        } else {
            Session::put('message', 'Tài khoản hoặc mật khẩu không chính xác');
            return Redirect::to('/xadmin/user');
        }
    }

    public function logout(Request $request){
        Session::put('username', null);
        Session::put('id', null);
        return Redirect::to('/xadmin/user');
    }
}
