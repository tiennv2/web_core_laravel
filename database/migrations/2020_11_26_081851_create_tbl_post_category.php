<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPostCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('alias');
            $table->string('description')->nullable();
            $table->text('content')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('image_medium', 255)->nullable();
            $table->string('image_thumb', 255)->nullable();
            $table->string('meta_url', 1000)->nullable();
            $table->string('meta_title', 1000)->nullable();
            $table->string('meta_keywords', 1000)->nullable();
            $table->string('meta_description', 1000)->nullable();
            $table->string('type', 255)->nullable();
            $table->integer('view', 11)->nullable();
            $table->tinyInteger('box_hot')->nullable();
            $table->tinyInteger('box_highlight')->nullable();
            $table->tinyInteger('box_home')->nullable();
            $table->tinyInteger('box_left')->nullable();
            $table->tinyInteger('box_footer')->nullable();
            $table->tinyInteger('box_right')->nullable();
            $table->string('parent', 255)->nullable();
            $table->string('left', 255)->nullable();
            $table->string('right', 255)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->integer('ordering')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_category');
    }
}
