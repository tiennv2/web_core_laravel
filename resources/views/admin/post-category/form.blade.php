@extends('admin_layout')
@section('admin_content')
    <div class="form-w3layouts">
        <div class="row">
            <section class="panel">
                <header class="panel-heading">Thêm danh mục</header>
                <form role="form" method="POST" action="{{URL::to('/xadmin/post-category/add')}}">
                    {{csrf_field()}}
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên danh mục</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Tên" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Danh mục</label>
                            <input type="text" class="form-control" id="description" placeholder="Password" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Hình ảnh</label>
                            <div class="input-group" id="open-file-image">
                                <input type="text" name="image" class="form-control" id="image" placeholder="Chọn hình ảnh" value="">
                                <span class="input-group-btn">
                                    <a href="javascript:;" onclick="javascript:openFile('image', 'images')" class="btn default">Chọn</a><a href="" onclick="javascript:fancyboxPreview('fancybox');" class="btn default preview fancybox" id="view_image" style="display: none;"><i class="fa fa-search-plus"></i></a><a onclick="javascript:removeFile('image');" class="btn default remove" id="remove_image" style="display: none;">
                                    <i class="fa fa-trash"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Chọn layout hiển thị
                            </label>
                            <select id="layout" name="layout" class="form-control m-bot15">
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                                <option value="">Option 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Thứ tự</label>
                            <input type="text" class="form-control" id="ordering" name="ordering" placeholder="Thứ tự" value="255"/>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Trạng thái</label>
                            <select id="status" name="status" class="form-control m-bot15">
                                <option value="1">Hiển thị</option>
                                <option value="0">Không hiển thị</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả</label>
                            <textarea type="text" class="form-control" id="description" name="description" placeholder="Mô tả"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Link thay thế</label>
                            <textarea type="text" class="form-control" id="meta_url" name="meta_url" placeholder="Mô tả"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Meta title</label>
                            <textarea type="text" class="form-control" id="meta_title" name="meta_title" placeholder="Mô tả"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Meta keywords</label>
                            <textarea type="text" class="form-control" id="meta_keywords" name="meta_keywords" placeholder="Mô tả"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="exampleInputFile">Meta description</label>
                            <textarea type="text" class="form-control" id="meta_description" name="meta_description" placeholder="Mô tả"></textarea>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hot</label>
                            <select id="box_hot" name="box_hot" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nổi bật</label>
                            <select id="box_highlight" name="box_highlight" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Trang chủ</label>
                            <select id="box_home" name="box_home" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Cuối trang</label>
                            <select id="box_footer" name="box_footer" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Cột trái</label>
                            <select id="box_left" name="box_left" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Cột phải</label>
                            <select id="box_right" name="box_right" class="form-control m-bot15">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-info" name="add_category">Submit</button>
                    </div>
                </form>
            </section>
            
        </div>
    </div>   
@endsection