<!DOCTYPE html>
<head>
<title>Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Visitors Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{asset('public/backend/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('public/backend/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('public/backend/css/style.css')}}" rel="stylesheet">
<link href="{{asset('public/backend/css/style-responsive.css')}}" rel="stylesheet">
<link href="{{asset('public/backend/css/font.css')}}" rel="stylesheet">
<link href="{{asset('public/backend/css/jquery2.0.3.min.css')}}" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="log-w3">
<div class="w3layouts-main">
	<h2>Đăng nhập quản trị Admin</h2>
	<?php
		$message = Session::get('message');
		if ($message) {
			echo '<div class="alert alert-danger display"><button class="close" data-close="alert"></button>'.$message.'</div>';
			Session::put('message', null);
		} 
	?>
	<form action="{{URL::to('/xadmin/login')}}" method="post">
		{{csrf_field()}}
		<input type="text" class="ggg" name="username" placeholder="Tên đăng nhập" required="">
		<input type="password" class="ggg" name="password" placeholder="Mật khẩu" required="">
		<span><input type="checkbox" />Nhớ lần đăng nhập kế tiếp</span>
		<h6><a href="#">Quên mật khẩu?</a></h6>
		<div class="clearfix"></div>
		<input type="submit" value="Đăng nhập" name="login">
	</form>
</div>
</div>
<script src="{{asset('public/backend/js/jquery.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('public/backend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{asset('public/backend/js/scripts.js')}}"></script>
<script src="{{asset('public/backend/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('public/backend/js/nicescroll.js')}}"></script>
<script src="{{asset('public/backend/js/scrollTo.js')}}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
</body>
</html>
